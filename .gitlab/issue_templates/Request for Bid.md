Instructions: This template is intended as a guide for creating a request for
bid issue to facilitate collecting bids for an approved project.

Replace any placeholders (text enclosed in parentheses) below as you complete
each section of the issue.  Be sure to remove this header section as well.

---

# Request for Bid

## Bids Reviewer

(Indicate who is going to review bids and select the winning bid.)

## Project Summary

(Briefly describe the project to be completed.)

## Project Details

(Provide sufficient additional details for a prospective bidder to be able to
develop a complete solution that meets all stated project goals and accurately
estimate the required effort.  Include links to the approved project proposal
and any other supporting documentation, such as interface specifications or
coding style guides, if applicable.)

## Guidelines For Prospective Bidders

(Give any guidelines which prospective bidders should follow in preparing a bid.
For example, a particular project may require "Analysis of Alternatives" as a
first milestone which must be costed separately and which must receive project
manager approval before other work can proceed.  Alternately, the funding
organization may specify a fixed price and the required bid format may simply be
a statement that the bidder can complete the work at the specified price.  If no
special conditions exist and no guidelines are needed, then a simple statement
to that effect is sufficient.)

/label ~"Request for bids"
